from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class AccountingPeriod(Document):
	def validate(self):
		self.validate_overlap()
	
	def validate_overlap(self):
		self.name = 1;